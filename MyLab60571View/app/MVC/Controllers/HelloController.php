<?php


namespace App\MVC\Controllers;
use Framework\Controller;

class HelloController extends Controller
{
    public function index(){
        return $this->view('index.php',['name'=>'Тимур']);
    }
    public function data(){
        return $this->view('data.php',['name'=>'Меркер Тимур Зайнудинович','group'=>'605-71']);
    }
}